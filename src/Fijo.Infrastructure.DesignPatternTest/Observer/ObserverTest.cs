﻿using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Observer.Impl;
using Fijo.Infrastructure.DesignPattern.Observer.Interface;
using Fijo.Infrastructure.DesignPattern.Observer.Intern;
using NUnit.Framework;

namespace Fijo.Infrastructure.DesignPatternTest.Observer {
	[TestFixture]
	public class ObserverTest {
		interface ILoggingObserverable : IObserverable {}

		interface ILoggingObserver : IObserver {
			IEnumerable<ILoggingObserverable> GetLog();
		}

		class LoggingObserver : ILoggingObserver {
			private readonly IList<ILoggingObserverable> _content = new List<ILoggingObserverable>();

			#region Implementation of IObserver
			public void Observe(IObserverable obj) {
				_content.Add((ILoggingObserverable) obj);
			}
			#endregion

			public IEnumerable<ILoggingObserverable> GetLog() {
				return _content;
			}
		}

		class LoggingObserverableContainer : ObserverableContainer<ILoggingObserverable>, ILoggingObserverable {
			public LoggingObserverableContainer(params ILoggingObserverable[] observerables) : base(observerables) {}
		}

		class ServiceLoggingObserverManager : ServiceObserverManager<ILoggingObserver, ILoggingObserverable> {
			public ServiceLoggingObserverManager(ILoggingObserverable observerableService, IObserverManager<ILoggingObserver, ILoggingObserverable> innerObserverManager) : base(observerableService, innerObserverManager) {}

			#region Overrides of ServiceObserverSubject<ILoggingObserver,ISimpleObserverable>
			protected override ILoggingObserverable CreateContainer(ILoggingObserverable observerableService, ILoggingObserverable obj) {
				return new LoggingObserverableContainer(observerableService, obj);
			}
			#endregion
		}

		class LoggingObserverableString : ObserverableString, ILoggingObserverable {
			public LoggingObserverableString(string content) : base(content) {}
		}

		class Service : IObserverableService<ILoggingObserver, ILoggingObserverable>, ILoggingObserverable {
			private readonly IObserverManager<ILoggingObserver, ILoggingObserverable> _observerManager;
			public Service(IObserverManager<ILoggingObserver, ILoggingObserverable> observerManager) {
				_observerManager = new ServiceLoggingObserverManager(this, observerManager);
			}

			public string Concat(string a, string b) {
				_observerManager.Notify(new LoggingObserverableString("Concat called"));
				_observerManager.Notify(new LoggingObserverableString(string.Format("Input arg ´a´ : {0}", a)));
				_observerManager.Notify(new LoggingObserverableString(string.Format("Input arg ´b´ : {0}", b)));

				var result = string.Concat(a, b);
				_observerManager.Notify(new LoggingObserverableString(string.Format("result: {0}", result)));
				return result;
			}

			#region Implementation of IObserverableServiceStructure<out IObserverSubject<ILoggingObserver,ISimpleObserverable>>
			IObserverManager<ILoggingObserver, ILoggingObserverable> IObserverableServiceStructure<IObserverManager<ILoggingObserver, ILoggingObserverable>>.GetObserverSubject() {
				return _observerManager;
			}
			#endregion
			#region Implementation of IObserverableServiceStructure<out IObserverSubject>
			IObserverManager IObserverableServiceStructure<IObserverManager>.GetObserverSubject() {
				return _observerManager;
			}
			#endregion
		}

		[Test]
		public void Test() {
			var observer = new LoggingObserver();
			var observerSubject = new ObserverManager<ILoggingObserver, ILoggingObserverable>(new[] {observer});
			var service = new Service(observerSubject);
			var result = service.Concat("hallo", "welt");

			var log = observer.GetLog();
			var expected = new List<ILoggingObserverable>
			{
				new LoggingObserverableContainer(service, new LoggingObserverableString("Concat called")),
				new LoggingObserverableContainer(service, new LoggingObserverableString("Input arg ´a´ : hallo")),
				new LoggingObserverableContainer(service, new LoggingObserverableString("Input arg ´b´ : welt")),
				new LoggingObserverableContainer(service, new LoggingObserverableString("result: hallowelt"))
			};
			CollectionAssert.AreEqual(expected, log);
		}

	}
}