﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Fijo.Infrastructure.DesignPattern.Repository;
using NUnit.Framework;

namespace Fijo.Infrastructure.DesignPatternTest.Repository {
	[TestFixture]
	public class SingletonRepositoryBaseTest {
		class SingeltonRepo : SingletonRepositoryBase<string> {
			private readonly IList<bool> _creationLog;

			public SingeltonRepo(IList<bool> creationLog) {
				_creationLog = creationLog;
			}
			#region Overrides of SingletonRepositoryBase<string>
			protected override string Create() {
				_creationLog.Add(true);
				return "test";
			}
			#endregion
		}

		[Test]
		 public void MultithreadingSave() {
			var creationLog = new List<bool>();
			var errors = new List<Exception>();
			var singeltonRepo = new SingeltonRepo(creationLog);
			var threads = Enumerable.Range(0, 32).Select(x => new Thread(() => {
				Console.WriteLine("Thread started");
				try {
					Assert.AreEqual(singeltonRepo.Get(), "test");
				}
				catch (Exception e) {
					errors.Add(e);
				}
				Console.WriteLine("Thread ended");
			})).ToList();
			Parallel.ForEach(threads, x => x.Start());
			Thread.Sleep(1000);
			threads.ForEach(x => x.Join());
			Assert.IsEmpty(errors);
			CollectionAssert.AreEqual(new List<bool> {true}, creationLog);
		}
	}
}