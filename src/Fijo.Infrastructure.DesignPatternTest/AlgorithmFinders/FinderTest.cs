﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Controller;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Exception;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Interface;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Objects;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Provider;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using NUnit.Framework;

namespace Fijo.Infrastructure.DesignPatternTest.AlgorithmFinders {
	[TestFixture]
	public class FinderTest {
		private IDictionary<Operations, IOperation> _operations;

		[SetUp]
		public void SetUp() {
			_operations = new IOperation[]
			{
				new AddOperation(),
				new SubtractOperation(),
				new DivideOperation(),
				new MultiplyOperation(),
				new UndefinedOperation()
			}.ToDictionary(x => x.Type, x => x);
		}

		[OnlyPublicFor("usage in the test methodes in this class")]
		public enum Operations {
			Add,
			Subtract,
			Divide,
			Multiply,
			Undefined
		}
		interface IOperation {
			Operations Type { get; }
			int Operation(int a, int b);
		}
		private class AddOperation : IOperation {
			public Operations Type { get { return Operations.Add; } }
			public int Operation(int a, int b) {
				return a + b;
			}
		}
		class SubtractOperation : IOperation {
			public Operations Type { get { return Operations.Subtract; } }
			public int Operation(int a, int b) {
				return a - b;
			}
		}
		class DivideOperation : IOperation {
			public Operations Type { get { return Operations.Divide; } }
			public int Operation(int a, int b) {
				return a / b;
			}
		}
		class MultiplyOperation : IOperation {
			public Operations Type { get { return Operations.Multiply; } }
			public int Operation(int a, int b) {
				return a * b;
			}
		}
		class UndefinedOperation : IOperation {
			public Operations Type { get { return Operations.Undefined; } }
			public int Operation(int a, int b) {
				return a;
			}
		}
		class FallbackOperation : IOperation {
			public Operations Type { get { throw new NotSupportedException(); } }
			public int Operation(int a, int b) {
				return a;
			}
		}
		class OperationProvider : IAlgorithmProvider<IOperation, Operations> {
			public Algorithms<IOperation, Operations> Get() {
				return new Algorithms<IOperation, Operations>(new IOperation[]
				{
					new AddOperation(),
					new SubtractOperation(),
					new DivideOperation(),
					new MultiplyOperation()
				}.ToDictionary(x => x.Type, x => x));
			}
		}
		class FallbackOperationProvider : IAlgorithmProvider<IOperation, Operations> {
			public Algorithms<IOperation, Operations> Get() {
				return new FallbackAlgorithms<IOperation, Operations>(new IOperation[]
				{
					new AddOperation(),
					new SubtractOperation(),
					new DivideOperation(),
					new MultiplyOperation()
				}.ToDictionary(x => x.Type, x => x), new FallbackOperation());
			}
		}
		class DashOperationProvider : IAlgorithmProvider<IOperation, Operations> {
			public Algorithms<IOperation, Operations> Get() {
				return new Algorithms<IOperation, Operations>(new IOperation[]
				{
					new AddOperation(),
					new SubtractOperation(),
				}.ToDictionary(x => x.Type, x => x));
			}
		}
		class SectionOperationProvider : IAlgorithmProvider<IOperation, string> {
			public Algorithms<IOperation, string> Get() {
				return new Algorithms<IOperation, string>(new IOperation[]
				{
					new AddOperation(),
					new SubtractOperation(),
					new DivideOperation(),
					new MultiplyOperation()
				}.ToDictionary(x => Enum.GetName(typeof(Operations), x.Type), x => x));
			}
		}
		class SectionFallbackOperationProvider : IAlgorithmProvider<IOperation, string> {
			public Algorithms<IOperation, string> Get() {
				return new FallbackAlgorithms<IOperation, string>(new IOperation[]
				{
					new AddOperation(),
					new SubtractOperation(),
					new DivideOperation(),
					new MultiplyOperation()
				}.ToDictionary(x => Enum.GetName(typeof(Operations), x.Type), x => x), new FallbackOperation());
			}
		}

		#region Key Finder
		[TestCase(Operations.Add, typeof(AddOperation))]
		[TestCase(Operations.Subtract, typeof(SubtractOperation))]
		[TestCase(Operations.Divide, typeof(DivideOperation))]
		[TestCase(Operations.Multiply, typeof(MultiplyOperation))]
		public void KnownOperation_FinderGet_MustReturnWantedOperationAlgorithm(Operations operationType, Type type) {
			var finder = new Finder<IOperation, Operations>(new OperationProvider());
			var operation = finder.Get(operationType);
			Assert.AreEqual(type, operation.GetType());
		}

		[TestCase(Operations.Undefined)]
		public void UnknownOperation_FinderGet_MustThrowGenericAlgorithmNotFoundException(Operations operationType) {
			var finder = new Finder<IOperation, Operations>(new OperationProvider());
			Assert.Throws<AlgorithmNotFoundException<Operations, IOperation>>(() => {
				var operation = finder.Get(operationType);
			});
		}

		private IDictionary<Operations, IOperation> GetAdditionalOperations(Operations addOperation1, Operations addOperation2) {
			return new[] {addOperation1, addOperation2}.ToDictionary(x => x, x => _operations[x]);
		}

		[TestCase(Operations.Add, Operations.Subtract, Operations.Add, typeof(AddOperation))]
		[TestCase(Operations.Add, Operations.Subtract, Operations.Subtract, typeof(SubtractOperation))]
		[TestCase(Operations.Add, Operations.Multiply, Operations.Add, typeof(AddOperation))]
		[TestCase(Operations.Add, Operations.Multiply, Operations.Subtract, typeof(SubtractOperation))]
		[TestCase(Operations.Add, Operations.Multiply, Operations.Multiply, typeof(MultiplyOperation))]
		[TestCase(Operations.Add, Operations.Divide, Operations.Add, typeof(AddOperation))]
		[TestCase(Operations.Add, Operations.Divide, Operations.Subtract, typeof(SubtractOperation))]
		[TestCase(Operations.Add, Operations.Divide, Operations.Divide, typeof(DivideOperation))]
		[TestCase(Operations.Subtract, Operations.Multiply, Operations.Add, typeof(AddOperation))]
		[TestCase(Operations.Subtract, Operations.Multiply, Operations.Subtract, typeof(SubtractOperation))]
		[TestCase(Operations.Subtract, Operations.Multiply, Operations.Multiply, typeof(MultiplyOperation))]
		[TestCase(Operations.Subtract, Operations.Divide, Operations.Add, typeof(AddOperation))]
		[TestCase(Operations.Subtract, Operations.Divide, Operations.Subtract, typeof(SubtractOperation))]
		[TestCase(Operations.Subtract, Operations.Divide, Operations.Divide, typeof(DivideOperation))]
		[TestCase(Operations.Multiply, Operations.Divide, Operations.Add, typeof(AddOperation))]
		[TestCase(Operations.Multiply, Operations.Divide, Operations.Subtract, typeof(SubtractOperation))]
		[TestCase(Operations.Multiply, Operations.Divide, Operations.Divide, typeof(DivideOperation))]
		[TestCase(Operations.Multiply, Operations.Divide, Operations.Multiply, typeof(MultiplyOperation))]
		public void KnownOperation_FinderGetWithAdditionalOperations_MustReturnWantedOperationAlgorithm(Operations addOperation1, Operations addOperation2, Operations operationType, Type type) {
			var finder = new Finder<IOperation, Operations>(new DashOperationProvider());
			var additionalOperations = GetAdditionalOperations(addOperation1, addOperation2);
			var operation = finder.Get(operationType, additionalOperations);
			Assert.AreEqual(type, operation.GetType());
		}
		
		[TestCase(Operations.Add, Operations.Subtract, Operations.Multiply)]
		[TestCase(Operations.Add, Operations.Subtract, Operations.Divide)]
		[TestCase(Operations.Add, Operations.Subtract, Operations.Undefined)]
		[TestCase(Operations.Add, Operations.Multiply, Operations.Divide)]
		[TestCase(Operations.Add, Operations.Multiply, Operations.Undefined)]
		[TestCase(Operations.Add, Operations.Divide, Operations.Multiply)]
		[TestCase(Operations.Add, Operations.Divide, Operations.Undefined)]
		[TestCase(Operations.Subtract, Operations.Multiply, Operations.Divide)]
		[TestCase(Operations.Subtract, Operations.Multiply, Operations.Undefined)]
		[TestCase(Operations.Subtract, Operations.Divide, Operations.Multiply)]
		[TestCase(Operations.Subtract, Operations.Divide, Operations.Undefined)]
		[TestCase(Operations.Multiply, Operations.Divide, Operations.Undefined)]
		public void UnknownOperation_FinderGet_MustThrowGenericAlgorithmNotFoundException(Operations addOperation1, Operations addOperation2, Operations operationType) {
			var finder = new Finder<IOperation, Operations>(new DashOperationProvider());
			var additionalOperations = GetAdditionalOperations(addOperation1, addOperation2);
			Assert.Throws<AlgorithmNotFoundException<Operations, IOperation>>(() => {
				var operation = finder.Get(operationType, additionalOperations);
			});
		}

		[TestCase(Operations.Undefined, typeof(FallbackOperation))]
		public void UnknownOperation_FinderGetWithFallbackOperation_MustReturnWantedOperationAlgorithm(Operations operationType, Type type) {
			var finder = new Finder<IOperation, Operations>(new FallbackOperationProvider());
			var operation = finder.Get(operationType);
			Assert.AreEqual(type, operation.GetType());
		}
		#endregion

		#region Section Finder
		class OperationsSelectionProvider : ISelectionProvider<Operations, string> {
			public Operations KeySelector(string section) {
				return (Operations) Enum.Parse(typeof (Operations), section);
			}

			public string SectionSelector(Operations key) {
				return Enum.GetName(typeof (Operations), key);
			}
		}
		
		[TestCase("Add", typeof(AddOperation))]
		[TestCase("Subtract", typeof(SubtractOperation))]
		[TestCase("Divide", typeof(DivideOperation))]
		[TestCase("Multiply", typeof(MultiplyOperation))]
		public void KnwonOperation_SectionFinderGetWithSectionOperationProvider_MustReturnWantedOperationAlgorithm(string operationType, Type type) {
			var finder = new Finder<IOperation, string, Operations>(new SectionOperationProvider(), new OperationsSelectionProvider());
			var operation = finder.Get(operationType);
			Assert.AreEqual(type, operation.GetType());
		}
		
		[TestCase("Add", typeof(AddOperation))]
		[TestCase("Subtract", typeof(SubtractOperation))]
		[TestCase("Divide", typeof(DivideOperation))]
		[TestCase("Multiply", typeof(MultiplyOperation))]
		public void KnwonOperation_SectionFinderGet_MustReturnWantedOperationAlgorithm(string operationType, Type type) {
			var finder = new Finder<IOperation, string, Operations>(new OperationProvider(), new OperationsSelectionProvider());
			var operation = finder.Get(operationType);
			Assert.AreEqual(type, operation.GetType());
		}

		[TestCase("Undefined")]
		public void UnknownOperationFromEnum_SectionFinderGet_MustThrowGenericAlgorithmNotFoundException(string operationType) {
			var finder = new Finder<IOperation, string, Operations>(new OperationProvider(), new OperationsSelectionProvider());
			Assert.Throws<AlgorithmNotFoundException<Operations, IOperation>>(() => {
				var operation = finder.Get(operationType);
			});
		}

		[TestCase(Operations.Add, Operations.Divide, "Add", typeof(AddOperation))]
		[TestCase(Operations.Add, Operations.Divide, "Subtract", typeof(SubtractOperation))]
		[TestCase(Operations.Add, Operations.Divide, "Divide", typeof(DivideOperation))]
		[TestCase(Operations.Multiply, Operations.Divide, "Multiply", typeof(MultiplyOperation))]
		public void KnownOperation_SectionFinderGetWithAdditionalOperations_MustReturnWantedOperationAlgorithm(Operations addOperation1, Operations addOperation2, string operationType, Type type) {
			var finder = new Finder<IOperation, string, Operations>(new DashOperationProvider(), new OperationsSelectionProvider());
			var additionalOperations = GetAdditionalOperations(addOperation1, addOperation2);
			var operation = finder.Get(operationType, additionalOperations);
			Assert.AreEqual(type, operation.GetType());
		}
		
		[TestCase(Operations.Subtract, Operations.Multiply, "Divide")]
		[TestCase(Operations.Subtract, Operations.Divide, "Multiply")]
		[TestCase(Operations.Multiply, Operations.Divide, "Undefined")]
		public void UnknownOperation_SectionFinderGet_MustThrowGenericAlgorithmNotFoundException(Operations addOperation1, Operations addOperation2, string operationType) {
			var finder = new Finder<IOperation, string, Operations>(new DashOperationProvider(), new OperationsSelectionProvider());
			var additionalOperations = GetAdditionalOperations(addOperation1, addOperation2);
			Assert.Throws<AlgorithmNotFoundException<Operations, IOperation>>(() => {
				var operation = finder.Get(operationType, additionalOperations);
			});
		}

		[TestCase("Undefined", typeof(FallbackOperation))]
		public void UnknownOperation_SectionFinderGetWithFallbackOperation_MustReturnWantedOperationAlgorithm(string operationType, Type type) {
			var finder = new Finder<IOperation, string, Operations>(new FallbackOperationProvider(), new OperationsSelectionProvider());
			var operation = finder.Get(operationType);
			Assert.AreEqual(type, operation.GetType());
		}

		[TestCase("Undefined", typeof(FallbackOperation))]
		public void UnknownOperation_SectionFinderGetWithSectionFallbackOperationProvider_MustReturnWantedOperationAlgorithm(string operationType, Type type) {
			var finder = new Finder<IOperation, string, Operations>(new SectionFallbackOperationProvider(), new OperationsSelectionProvider());
			var operation = finder.Get(operationType);
			Assert.AreEqual(type, operation.GetType());
		}
		#endregion
	}
}