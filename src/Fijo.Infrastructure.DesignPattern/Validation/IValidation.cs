using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Validation {
	[PublicAPI, Service]
	[Desc("This service should be wrapped around a entity by creating a class that inherits from this IValidation<EntityType> - EntityType should be the your Entity you wana validate. Here you should impl your validation logic")]
	[Note("It is recomented to use an implementation as nested class of the entity that is used")]
	public interface IValidation<in T> {
		[PublicAPI] bool IsValid(T obj);
	}
}