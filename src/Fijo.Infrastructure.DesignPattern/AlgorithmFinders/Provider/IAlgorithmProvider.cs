using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Objects;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Provider {
	[PublicAPI]
	public interface IAlgorithmProvider<TAlgorithm, TKey> {
		Algorithms<TAlgorithm, TKey> Get();
	}
}