using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using SException = System.Exception;

namespace Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Exception {
	[PublicAPI, Serializable]
	public class AlgorithmNotFoundException : KeyNotFoundException {
		public AlgorithmNotFoundException(string message = default(string), SException innerException = null) : base(message, innerException) {}
	}

	[PublicAPI, Serializable]
	public class AlgorithmNotFoundException<TKey, TAlgorithm> : AlgorithmNotFoundException {
		public object Finder { get; set; }
		public IList<KeyValuePair<TKey, TAlgorithm>> Algorithms { get; set; }
		public TKey Key { get; set; }
		public object Section { get; set; }

		public AlgorithmNotFoundException(object finder, IList<KeyValuePair<TKey, TAlgorithm>> algorithms, TKey key, object section = null, string message = default(string), SException innerException = null) : base(message, innerException) {
			Finder = finder;
			Algorithms = algorithms;
			Key = key;
			Section = section;
		}
	}
}