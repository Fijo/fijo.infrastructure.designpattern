using System.Collections.Generic;

namespace Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Objects {
	public class FallbackAlgorithms<TAlgorithm, TKey> : Algorithms<TAlgorithm, TKey> {
		public readonly TAlgorithm Fallback;

		public FallbackAlgorithms(IEnumerable<KeyValuePair<TKey, TAlgorithm>> content, TAlgorithm fallback) : base(content) {
			Fallback = fallback;
		}
	}
}