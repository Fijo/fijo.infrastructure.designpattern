using System.Collections.Generic;

namespace Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Objects {
	public class Algorithms<TAlgorithm, TKey> {
		public readonly IEnumerable<KeyValuePair<TKey, TAlgorithm>> Content;

		public Algorithms(IEnumerable<KeyValuePair<TKey, TAlgorithm>> content) {
			Content = content;
		}
	}
}