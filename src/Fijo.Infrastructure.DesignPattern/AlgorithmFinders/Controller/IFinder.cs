using System.Collections.Generic;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Controller {
	public interface IFinder<TAlgorithm, in TSection, TKey> : IFinder<TAlgorithm, TKey> {
		[PublicAPI, Pure] TAlgorithm Get(TSection section, IDictionary<TKey, TAlgorithm> algorithms = null);
	}

	public interface IFinder<TAlgorithm, TKey> {
		[PublicAPI, Pure] TAlgorithm Get(TKey key, IDictionary<TKey, TAlgorithm> algorithms = null);
	}
}