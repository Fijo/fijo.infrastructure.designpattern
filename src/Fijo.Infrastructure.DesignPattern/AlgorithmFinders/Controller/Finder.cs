using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Exception;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Interface;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Objects;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Provider;
using Fijo.Infrastructure.DesignPattern.Extentions;
using Fijo.Infrastructure.Documentation.Attributes.Annotation;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Controller {
	public class Finder<TAlgorithm, TSection, TKey> : Finder<TAlgorithm, TKey>, IFinder<TAlgorithm, TSection, TKey> {
		protected readonly ISelectionProvider<TKey, TSection> SelectionProvider;
		
		public Finder(IAlgorithmProvider<TAlgorithm, TKey> algorithmProvider, ISelectionProvider<TKey, TSection> selectionProvider) : base(algorithmProvider) {
			SelectionProvider = selectionProvider;
		}

		public Finder(IAlgorithmProvider<TAlgorithm, TSection> algorithmProvider, ISelectionProvider<TKey, TSection> selectionProvider) {
			SelectionProvider = selectionProvider;
			var algorithms = algorithmProvider.Get();
			Algorithms = GetAlgorithms(algorithms, selectionProvider.KeySelector);
			InitFallback(algorithms, out UseFallback, out Fallback);
		}

		protected new class FinderRequest : Finder<TAlgorithm, TKey>.FinderRequest {
			public TSection Section;
			public bool UseSection;
		}
		
		#region Implementation of IFinder<TAlgorithm,in TSection,TKey>
		public TAlgorithm Get(TSection section, IDictionary<TKey, TAlgorithm> algorithms = null) {
			TAlgorithm result;
			var key = SelectionProvider.KeySelector(section);
			return TryGet(algorithms, key, out result)
				       ? result
				       : GetFallbackAlgorithm(new FinderRequest {Algorithms = algorithms, Key = key, Section = section, UseSection = true});
		}
		#endregion

		protected override AlgorithmNotFoundException GetNotFoundException(Finder<TAlgorithm, TKey>.FinderRequest fr) {
			var finderRequest = (FinderRequest) fr;
			#region PreCondition
			Debug.Assert(!finderRequest.Key.IsUndefined(), "!finderRequest.Key.IsUndefined()");
			Debug.Assert(!finderRequest.UseSection || !finderRequest.Section.IsUndefined(), "!finderRequest.UseSection || !finderRequest.Section.IsUndefined()");
			#endregion
			var type = GetType();
			var key = finderRequest.Key;
			var algorithms = finderRequest.Algorithms;
			var usedAlgorithms = GetUsedAlgorithms(algorithms).ToList();
			return finderRequest.UseSection
				       ? new AlgorithmNotFoundException<TKey, TAlgorithm>(type, usedAlgorithms, key, finderRequest.Section)
				       : new AlgorithmNotFoundException<TKey, TAlgorithm>(type, usedAlgorithms, key);
		}
	}

	public class Finder<TAlgorithm, TKey> : IFinder<TAlgorithm, TKey> {
		[UsedReadonly] protected IDictionary<TKey, TAlgorithm> Algorithms;
		[UsedReadonly] protected bool UseFallback;
		[UsedReadonly] protected TAlgorithm Fallback;

		public Finder(IAlgorithmProvider<TAlgorithm, TKey> algorithmProvider) {
			var algorithms = algorithmProvider.Get();
			InitFallback(algorithms, out UseFallback, out Fallback);
			Algorithms = GetAlgorithms(algorithms, x => x);
		}

		[Note("You have to call InitFallback() and set Algorithms, UseFallback (to the value of InitFallback�s of useFallback out param), Fallback (to the value of InitFallback�s of fallback out param) in the overwriting constructor when using this constructor")]
		protected Finder() {}

		protected IDictionary<TKey, TAlgorithm> GetAlgorithms<TInternalKey>(Algorithms<TAlgorithm, TInternalKey> algorithms, Func<TInternalKey, TKey> selector) {
			return algorithms.Content.ToDictionary(x => selector(x.Key), x => x.Value);
		}

		protected void InitFallback<TInternalKey>(Algorithms<TAlgorithm, TInternalKey> algorithms, out bool useFallback, out TAlgorithm fallback) {
			Debug.Assert(algorithms != null);
			var fallbackAlgorithms = algorithms as FallbackAlgorithms<TAlgorithm, TInternalKey>;
			useFallback = fallbackAlgorithms != null;
			fallback = useFallback ? fallbackAlgorithms.Fallback : default(TAlgorithm);
		}

		[Dto]
		protected class FinderRequest {
			public IDictionary<TKey, TAlgorithm> Algorithms;
			public TKey Key;
		}
		
		#region Get
		#region Implementation of IFinder<TAlgorithm,TKey>
		public virtual TAlgorithm Get(TKey key, IDictionary<TKey, TAlgorithm> algorithms = null) {			
			TAlgorithm result;
			return TryGet(algorithms, key, out result)
				       ? result
				       : GetFallbackAlgorithm(new FinderRequest {Algorithms = algorithms, Key = key});
		}
		#endregion

		protected bool TryGet(IDictionary<TKey, TAlgorithm> algorithms, TKey key, out TAlgorithm result) {
			return (algorithms != null && algorithms.TryGetValue(key, out result))
			       || Algorithms.TryGetValue(key, out result);
		}

		protected virtual TAlgorithm GetFallbackAlgorithm(FinderRequest finderRequest) {
			if(UseFallback) return Fallback;
			throw GetNotFoundException(finderRequest);
		}
		#endregion
		
		protected IEnumerable<KeyValuePair<TKey, TAlgorithm>> GetUsedAlgorithms(IEnumerable<KeyValuePair<TKey, TAlgorithm>> algorithms) {
			return algorithms != null ? Algorithms.Concat(algorithms) : Algorithms;
		}

		protected virtual AlgorithmNotFoundException GetNotFoundException(FinderRequest finderRequest) {
			#region PreCondition
			Debug.Assert(!finderRequest.Key.IsUndefined(), "key.IsUndefined()");
			#endregion
			return new AlgorithmNotFoundException<TKey, TAlgorithm>(GetType(), GetUsedAlgorithms(finderRequest.Algorithms).ToList(),
			                                                        finderRequest.Key);
		}
	}
}