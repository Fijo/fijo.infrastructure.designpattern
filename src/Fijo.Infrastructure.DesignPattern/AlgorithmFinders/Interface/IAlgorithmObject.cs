using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Interface {
	[PublicAPI]
	public interface IAlgorithmObject<out TAlgorithm, out TSection> {
		[PublicAPI] TSection Section { get; }
		[PublicAPI] TAlgorithm Algorithm { get; }
	}

	[PublicAPI]
	public interface IAlgorithmObject {
		[PublicAPI] object Section { get; }
		[PublicAPI] object Algorithm { get; }
	}
}