namespace Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Interface
{
	public interface ISelectionProvider<TKey, TSection> {
		TKey KeySelector(TSection section);
		TSection SectionSelector(TKey key);
	}
}