using System.ComponentModel;
using Fijo.Infrastructure.DesignPattern.Accessor.Delegates;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Accessor {
	[PublicAPI]
	public interface IAccessor<in TInstance, TValue> {
		[PublicAPI, EditorBrowsable(EditorBrowsableState.Never)] Getter<TInstance, TValue> Getter { get; }
		[PublicAPI, EditorBrowsable(EditorBrowsableState.Never)] Setter<TInstance, TValue> Setter { get; }
	}

	[PublicAPI]
	public interface IAccessor<TValue> {
		[PublicAPI, EditorBrowsable(EditorBrowsableState.Never)] Getter<TValue> Getter { get; }
		[PublicAPI, EditorBrowsable(EditorBrowsableState.Never)] Setter<TValue> Setter { get; }
	}
}