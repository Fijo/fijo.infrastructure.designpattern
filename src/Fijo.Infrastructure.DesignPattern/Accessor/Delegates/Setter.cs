using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Accessor.Delegates {
	[PublicAPI]
	public delegate void Setter<in TInstance, in TValue>(TInstance me, TValue value);

	[PublicAPI]
	public delegate void Setter<in TValue>(TValue value);
}