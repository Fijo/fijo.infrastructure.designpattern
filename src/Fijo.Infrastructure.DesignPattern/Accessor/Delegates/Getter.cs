using Fijo.Infrastructure.Documentation.Attributes.Annotation;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Accessor.Delegates {
	[DelPure, PublicAPI]
	public delegate TValue Getter<out TValue>();

	[DelPure, PublicAPI]
	public delegate TValue Getter<in TInstance, out TValue>(TInstance me);
}