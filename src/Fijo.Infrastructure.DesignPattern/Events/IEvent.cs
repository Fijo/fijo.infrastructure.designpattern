namespace Fijo.Infrastructure.DesignPattern.Events {
	public interface IEvent {
		ulong Revision { get; set; }
	}
}