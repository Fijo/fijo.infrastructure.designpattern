using System;

namespace Fijo.Infrastructure.DesignPattern.Events {
	[Serializable]
	public abstract class Event : IEvent {
		#region Implementation of IEvent
		public ulong Revision { get; set; }
		#endregion
	}
}