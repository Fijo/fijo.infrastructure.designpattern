using System;
using System.Collections;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Exceptions {
	[PublicAPI, Serializable]
	public class NonUniqueSequenceException : Exception {
		[CanBeNull, PublicAPI]
		public ICollection SourceSequence { get; protected set; }

		public NonUniqueSequenceException(ICollection sourceSequence = null, string message = default(string), Exception exception = null) : base(message, exception) {
			SourceSequence = sourceSequence;
		}
	}
}