﻿using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Extentions {
	internal static class IsUndefinedExtention {
		[Pure, DebuggerStepThrough, Desc("Compares T with null being shure that it do not compare a value type with null.")]
		internal static bool IsUndefined<T>(this T me) {
			return !typeof(T).IsValueType && Equals(null, me);
		}
	}
}