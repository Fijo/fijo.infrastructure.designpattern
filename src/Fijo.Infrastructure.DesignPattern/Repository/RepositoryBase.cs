namespace Fijo.Infrastructure.DesignPattern.Repository {
	public abstract class RepositoryBase<T> : IRepository<T> {
		#region Implementation of IRepository
		public abstract T Get();

		object IRepository.Get() {
			return Get();
		}
		#endregion
	}
}