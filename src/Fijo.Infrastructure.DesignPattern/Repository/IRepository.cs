﻿using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Repository {
	[PublicAPI, Repository]
	public interface IRepository {
		[PublicAPI] object Get();
	}

	[PublicAPI]
	public interface IRepository<out T> : IRepository {
		[PublicAPI] new T Get();
	}
}