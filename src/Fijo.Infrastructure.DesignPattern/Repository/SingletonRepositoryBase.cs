using System;
using System.Diagnostics;
using System.Threading;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Repository {
	[Desc("a repository that provides you to override a Create function that is only called once and that returns the value that is used to be returnd when calling Get.")]
	[Note("I don�t execute create in the constructor anymore, because I got the problem, I do something in the constructor, of an inherited class, the constructor will be executed after the one in this class (its base constructor), so for example the fields have not the values you would expect. So I decided to call Create() later.")]
	public abstract class SingletonRepositoryBase<T> : RepositoryBase<T> {
		private readonly AutoResetEvent _contentLock = new AutoResetEvent(true);
		private T _content;
		private bool _isCreated;
		[Note("isInited means, that Create() has been executed. It is like _isCreated but it is algough true if a exception occurred while executing Create() and IsCorrupt is true. So the value of it should always be equal with �_isCreated || IsCorrupt�")]
		private bool _isInited;
		[Desc("Exception that is used if the repository has a corrupt state, that can be caused by Exceptions while execution Create().")]
		private DataProvisionException _corruptStateException;
		private bool IsCorrupt { get { return _corruptStateException != null; } }

		protected abstract T Create();

		#region Overrides of RepositoryBase<T>
		public override T Get() {
			if(!_isInited) {
				_contentLock.WaitOne();
				if(_isInited) _contentLock.Set();
			}
			if (_isCreated) return _content;
			if (IsCorrupt) throw _corruptStateException;
			Debug.Assert(!_isInited);
			try {
				_content = Create();
			}
			catch (Exception e) {
				throw (_corruptStateException = new DataProvisionException(this, "A exception occurred while doing the one time data resolve in a SingletonRepository.", e));
			}
			finally {
				_isInited = true;
				_contentLock.Set();
			}
			_isCreated = true;
			return _content;
		}
		#endregion
	}

	[Serializable, PublicAPI]
	public class DataProvisionException : InvalidOperationException {
		public object Provider { get; set; }

		public DataProvisionException(object provider, string message = default(string), Exception innerException = null) : base(message, innerException) {
			Provider = provider;
		}
	}
}