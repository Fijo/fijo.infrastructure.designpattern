using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Repository.Collection {
	[PublicAPI]
	public abstract class CollectionRepositoryBase<T> : InternalCollectionRepositoryBase, ICollectionRepository<T> {
		#region Overrides of CollectionRepositoryBase
		protected override IEnumerable InternalGet() {
			return GetAll();
		}

		IEnumerable<T> ICollectionRepository<T>.Get() {
			return GetAll();
		}

		IEnumerable<T> IRepository<IEnumerable<T>>.Get() {
			return GetAll();
		}

		protected abstract IEnumerable<T> GetAll();
		#endregion
	}

	public abstract class CollectionRepositoryBase : InternalCollectionRepositoryBase {
		#region Overrides of InternalCollectionRepositoryBase
		protected abstract IEnumerable GetAll();

		protected override IEnumerable InternalGet() {
			return GetAll();
		}
		#endregion
	}
}