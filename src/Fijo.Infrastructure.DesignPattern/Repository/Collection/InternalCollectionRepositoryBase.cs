using System.Collections;

namespace Fijo.Infrastructure.DesignPattern.Repository.Collection {
	public abstract class InternalCollectionRepositoryBase : ICollectionRepository {
		#region Implementation of IRepository
		protected abstract IEnumerable InternalGet();

		IEnumerable IRepository<IEnumerable>.Get() {
			return InternalGet();
		}

		object IRepository.Get() {
			return InternalGet();
		}
		#endregion
	}
}