using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Repository.Collection {
	[PublicAPI]
	public interface ICollectionRepository<out T> : ICollectionRepository, IRepository<IEnumerable<T>> {
		[PublicAPI] new IEnumerable<T> Get();
	}

	[PublicAPI]
	public interface ICollectionRepository : IRepository<IEnumerable> {}
}