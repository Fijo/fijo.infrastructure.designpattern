using System;
using Fijo.Infrastructure.DesignPattern.Lazy.Base;
using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Lazy {
	[PublicAPI]
	public class LazyField<T> : LazyFieldBase<T>, ILazyField<T> {
		private readonly Func<T> _resolverFunc;

		public LazyField(Func<T> resolverFunc) {
			_resolverFunc = resolverFunc;
		}

		protected override T Resolve() {
			return _resolverFunc();
		}
	}
}