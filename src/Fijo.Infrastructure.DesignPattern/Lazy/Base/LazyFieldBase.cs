using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Lazy.Base {
	[PublicAPI]
	public abstract class LazyFieldBase<T> : ILazy<T> {
		private T _store;
		private bool _hasSet;

		public T Get() {
			if (!_hasSet)
				lock (this) {
					_store = Resolve();
					_hasSet = true;
				}
			return _store;
		}

		protected abstract T Resolve();
	}
}