using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Lazy.Interface {
	[PublicAPI]
	public interface ILazyField<out T> : ILazy<T> {}
}