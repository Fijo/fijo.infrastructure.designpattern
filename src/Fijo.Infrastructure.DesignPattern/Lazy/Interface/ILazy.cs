using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Lazy.Interface {
	[PublicAPI]
	public interface ILazy<out T> {
		[PublicAPI] T Get();
	}
}