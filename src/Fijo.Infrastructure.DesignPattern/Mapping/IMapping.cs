﻿using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Mapping {
	[PublicAPI]
	public interface IMapping<in TSource, out TResult> {
		[PublicAPI]
		TResult Map(TSource source);
	}

	[PublicAPI]
	public interface IMapping<in TContext, in TSource, out TResult> {
		[PublicAPI]
		TResult Map(TContext context, TSource source);
	}
}