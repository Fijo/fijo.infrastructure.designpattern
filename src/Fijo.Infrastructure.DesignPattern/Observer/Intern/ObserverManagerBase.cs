using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fijo.Infrastructure.DesignPattern.Exceptions;
using Fijo.Infrastructure.Documentation.Attributes.Annotation;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Observer.Intern {
	[InternAbstraction]
	[ThreadSafety(ThreadSafety.Save)]
	public abstract class ObserverManagerBase<TObserver, TObserverable> {
		[NotNull] private readonly ISet<TObserver> _observers;

		protected internal ObserverManagerBase([NotNull] IEnumerable<TObserver> observers) {
			_observers = new HashSet<TObserver>();
			if(!observers.All(x => _observers.Add(x))) throw new NonUniqueSequenceException();
		}

		#region Implementation of IObserverSubject
		public void Attach([NotNull] TObserver observer) {
			lock(_observers) if(!_observers.Add(observer)) throw new ArgumentException("The observer you used is already attached", "observer");
		}

		public void Detach([NotNull] TObserver observer) {
			lock(_observers) if(!_observers.Remove(observer)) throw new KeyNotFoundException("The observer you wana detach is not attached. Argument ŽobserverŽ.");
		}

		public virtual void Notify([NotNull] TObserverable obj) {
			lock(_observers) Parallel.ForEach(_observers, x => ObserveNotification(x, obj));
		}

		protected abstract void ObserveNotification([NotNull] TObserver observer, [NotNull] TObserverable obj);
		#endregion
	}
}