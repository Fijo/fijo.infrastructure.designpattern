using Fijo.Infrastructure.Documentation.Attributes.Annotation;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Observer.Intern {
	[InternAbstraction]
	public interface IObserverableServiceStructure<out TObserverSubject> {
		[NotNull, PublicAPI]
		TObserverSubject GetObserverSubject();
	}
}