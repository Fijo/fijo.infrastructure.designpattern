using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Observer.Interface;
using Fijo.Infrastructure.DesignPattern.Observer.Intern;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Observer.Impl {
	[PublicAPI]
	public class ObserverManager<TObserver, TObserverable>
		: ObserverManagerBase<TObserver, TObserverable>, IObserverManager<TObserver, TObserverable>
		where TObserver : IObserver
		where TObserverable : IObserverable {
		public ObserverManager([NotNull] IEnumerable<TObserver> observers) : base(observers) {}

		#region Implementation of IObserverSubjectStructure<in IObserver,in IObserverable>
		public void Attach(IObserver observer) {
			base.Attach((TObserver) observer);
		}

		public void Detach(IObserver observer) {
			base.Detach((TObserver) observer);
		}

		public void Notify(IObserverable obj) {
			base.Detach((TObserver) obj);
		}
		#endregion

		#region Overrides of ObserverManagerBase<TObserver,TObserverable>
		protected override void ObserveNotification(TObserver observer, TObserverable obj) {
			observer.Observe(obj);
		}
		#endregion
	}
}