using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Observer.Interface;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Observer.Impl {
	[PublicAPI]
	public class ObserverableContainer<TObserverable> where TObserverable : IObserverable {
		[NotNull] private readonly ICollection<TObserverable> _observerables;

		public ObserverableContainer([NotNull] params TObserverable[] observerables) {
			_observerables = observerables;
		}

		[NotNull, PublicAPI]
		public ICollection<TObserverable> GetContent() {
			return _observerables;
		}

		#region Equals
		public override bool Equals([NotNull] object obj) {
			#region PreCondition
			Debug.Assert(obj != null, "obj != null");
			#endregion
			return obj is ObserverableContainer<TObserverable> && Equals((ObserverableContainer<TObserverable>) obj);
		}

		[PublicAPI]
		protected bool Equals([NotNull] ObserverableContainer<TObserverable> other) {
			return _observerables.SequenceEqual(other.GetContent());
		}
		#endregion

		#region GetHashCode
		public override int GetHashCode() {
			if(!_observerables.Any()) return 0;
			return _observerables.Skip(1).Aggregate(_observerables.First().GetHashCode(),
			                                       (a, x) => unchecked((a * 397) ^ x.GetHashCode()));
		}
		#endregion

		#region ToString
		public override string ToString() {
			return string.Join(" => ", _observerables.Select(x => x.ToString()));
		}
		#endregion
	}
}