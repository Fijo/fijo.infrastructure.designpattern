using System.Diagnostics;
using Fijo.Infrastructure.DesignPattern.Observer.Interface;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Observer.Impl {
	[PublicAPI]
	public class ObserverableString : IObserverable {
		private readonly string _content;

		public ObserverableString(string content) {
			_content = content;
		}

		public override bool Equals([NotNull] object obj) {
			#region PreCondition
			Debug.Assert(obj != null, "obj != null");
			#endregion
			return obj is ObserverableString && Equals((ObserverableString) obj);
		}

		[PublicAPI]
		protected bool Equals([NotNull] ObserverableString other) {
			return _content == other.ToString();
		}

		public override int GetHashCode() {
			return _content.GetHashCode();
		}

		public override string ToString() {
			return _content;
		}
	}
}