using System.Diagnostics;
using Fijo.Infrastructure.DesignPattern.Observer.Interface;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Observer.Impl {
	[PublicAPI]
	public abstract class ServiceObserverManager<TObserver, TObserverable>
		: IObserverManager<TObserver, TObserverable>
		where TObserver : IObserver
		where TObserverable : IObserverable {
		[NotNull] private readonly TObserverable _observerableService;
		[NotNull] private readonly IObserverManager<TObserver, TObserverable> _innerObserverManager;

		protected ServiceObserverManager([NotNull] TObserverable observerableService, [NotNull] IObserverManager<TObserver, TObserverable> innerObserverManager) {
			#region PreCondition
			Debug.Assert(observerableService is IObserverableService<TObserver, TObserverable>, "observerableService is IObserverableService<TObserver, TObserverable>");
			#endregion
			_observerableService = observerableService;
			_innerObserverManager = innerObserverManager;
		}

		#region Implementation of IObserverSubjectStructure<in IObserver,in IObserverable>
		public void Attach(IObserver observer) {
			_innerObserverManager.Attach(observer);
		}

		public void Detach(IObserver observer) {
			_innerObserverManager.Detach(observer);
		}

		public void Notify(IObserverable obj) {
			Notify((TObserverable) obj);
		}
		#endregion
		#region Implementation of IObserverSubjectStructure<in TObserver,in TObserverable>
		public void Attach(TObserver observer) {
			_innerObserverManager.Attach(observer);
		}

		public void Detach(TObserver observer) {
			_innerObserverManager.Detach(observer);
		}

		public void Notify(TObserverable obj) {
			_innerObserverManager.Notify(CreateContainer(_observerableService, obj));
		}

		protected abstract TObserverable CreateContainer(TObserverable observerableService, TObserverable obj);
		#endregion
	}
}