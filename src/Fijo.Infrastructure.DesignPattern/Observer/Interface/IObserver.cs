﻿using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Infrastructure.DesignPattern.Observer.Interface {
	[Observer]
	public interface IObserver {
		[About("Watch")]
		void Observe(IObserverable obj);
	}
}