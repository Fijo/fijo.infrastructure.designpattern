using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace Fijo.Infrastructure.DesignPattern.Observer.Interface {
	[Observerable]
	public interface IObserverable {}
}