using Fijo.Infrastructure.DesignPattern.Observer.Intern;

namespace Fijo.Infrastructure.DesignPattern.Observer.Interface {
	public interface IObserverableService
		: IObserverableServiceStructure<IObserverManager>, IObserverable {}

	public interface IObserverableService<in TObserver, in TObserverable>
		: IObserverableServiceStructure<IObserverManager<TObserver, TObserverable>>, IObserverableService
		where TObserver : IObserver
		where TObserverable : IObserverable {}
}