using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace Fijo.Infrastructure.DesignPattern.Observer.Interface {
	public interface IObserverManager<in TObserver, in TObserverable> : IObserverManager
		where TObserver : IObserver
		where TObserverable : IObserverable {
		void Attach(TObserver observer);
		void Detach(TObserver observer);
		void Notify(TObserverable obj);
	}

	[ObserverManager]
	public interface IObserverManager {
		void Attach(IObserver observer);
		void Detach(IObserver observer);
		void Notify(IObserverable obj);
	}
}