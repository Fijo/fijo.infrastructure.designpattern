namespace Fijo.Infrastructure.DesignPattern.Store {
	public abstract class DefaultStore<TEntity, TKey, TStoreType> : DefaultStoreBase<TEntity, TKey, TStoreType> {}

	public abstract class DefaultStore<TEntity, TStoreType> : DefaultStore<TEntity, object, TStoreType> {}
}