using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Store {
	[PublicAPI, Serializable]
	public class EnitiyKeyNotFoundInStoreException : KeyNotFoundException {
		[NotNull, PublicAPI]
		public IStore Store { get; set; }
		[NotNull, PublicAPI]
		public object Key { get; set; }

		public EnitiyKeyNotFoundInStoreException([NotNull] IStore store, [NotNull] object key) : base(string.Format("Key �{0}� in store {1}", key, store.GetType().Name)) {
			Store = store;
			Key = key;
		}
	}
}