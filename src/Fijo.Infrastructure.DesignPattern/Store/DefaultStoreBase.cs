using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Store {
	[PublicAPI]
	public abstract class DefaultStoreBase<TEntity, TKey, TStoreType> : StoreBase<TEntity, TKey> {
		[NotNull] private readonly TStoreType _content;

		protected DefaultStoreBase() {
			// ReSharper disable DoNotCallOverridableMethodsInConstructor
			_content = CreateStore();
			// ReSharper restore DoNotCallOverridableMethodsInConstructor
		}

		#region Create
		[NotNull, PublicAPI]
		protected abstract TStoreType CreateStore();
		#endregion

		#region TryGet, Set
		[PublicAPI]
		protected abstract bool TryGet([NotNull] TStoreType content, [NotNull] TKey key, out TEntity result);

		[PublicAPI]
		protected abstract TEntity Set([NotNull] TStoreType content, [NotNull] TKey key, [NotNull] TEntity value);
		#endregion

		#region Get
		[PublicAPI]
		protected virtual TEntity Get([NotNull] TStoreType content, [NotNull] TKey key) {
			TEntity result;
			return TryGet(content, key, out result)
			       	? result
			       	: GetNotFound(content, key);
		}

		[PublicAPI]
		protected virtual TEntity GetNotFound([NotNull] TStoreType content, [NotNull] TKey key)	{
			throw new EnitiyKeyNotFoundInStoreException(this, key);
		}
		#endregion

		#region Get, Store
		public override TEntity Get(TKey key) {
			return Get(_content, key);
		}

		public override void Store(TEntity entity) {
			Set(_content, GetKey(entity), entity);
		}
		#endregion

		[PublicAPI]
		public abstract TKey GetKey([NotNull] TEntity entity);
	}
}