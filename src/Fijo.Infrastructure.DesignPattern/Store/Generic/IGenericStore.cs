﻿using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Store.Generic {
	[PublicAPI]
	public interface IGenericStore<[Note("nongeneric")] TBaseType, in TKey> : IStore<TBaseType, TKey> {
		TResult Get<TResult>(TKey key) where TResult : TBaseType;
	}
}