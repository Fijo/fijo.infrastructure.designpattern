using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Store.Generic {
	[PublicAPI]
	public abstract class GenericStore<TBaseType, TKey> : DictStore<TBaseType, TKey>, IGenericStore<TBaseType, TKey> {
		#region Implementation of IGenericStore<TBaseType,in TKey>
		public TResult Get<TResult>(TKey key) where TResult : TBaseType {
			return Cast<TResult>(Get(key));
		}
		#endregion

		[Pure]
		protected virtual TResult Cast<TResult>(TBaseType obj) where TResult : TBaseType  {
			return (TResult) obj;
		}
	}
}