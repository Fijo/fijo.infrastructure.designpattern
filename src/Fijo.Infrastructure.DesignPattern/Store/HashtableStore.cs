namespace Fijo.Infrastructure.DesignPattern.Store {
	public class HashtableStore<TEntity> : DictStore<TEntity, TEntity> {
		#region Overrides of DefaultStoreBase<TEntity,TEntity,IDictionary<TEntity,TEntity>>
		public override TEntity GetKey(TEntity entity) {
			return entity;
		}
		#endregion
	}
}