﻿using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Store {
	[PublicAPI, Store]
	public interface IStore {
		[PublicAPI] object Get([NotNull] object key);
		[PublicAPI] void Store([NotNull] object entity);
	}

	[PublicAPI]
	public interface IStore<TEntity> : IStore {
		[PublicAPI] new TEntity Get([NotNull] object key);
		[PublicAPI] void Store([NotNull] TEntity entity);
	}

	[PublicAPI]
	public interface IStore<TEntity, in TKey> : IStore<TEntity> {
		[PublicAPI] TEntity Get([NotNull] TKey key);
	}
}