using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Infrastructure.DesignPattern.Store {
	[AboutName("AI", "auto inited")]
	public abstract class AIDefaultStore<TEntity, TKey, TStoreType> : DefaultStoreBase<TEntity, TKey, TStoreType> where TStoreType : new() {}

	[AboutName("AI", "auto inited")]
	public abstract class AIDefaultStore<TEntity, TStoreType> : AIDefaultStore<TEntity, object, TStoreType> where TStoreType : new() {}
}