using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Store {
	[PublicAPI, Store]
	public interface IBasicStore {
		[PublicAPI] object Get();
		[PublicAPI] void Store([NotNull] object entity);
	}

	[PublicAPI]
	public interface IBasicStore<TEntity> : IBasicStore {
		[PublicAPI] new TEntity Get();
		[PublicAPI] void Store([NotNull] TEntity entity);
	}
}