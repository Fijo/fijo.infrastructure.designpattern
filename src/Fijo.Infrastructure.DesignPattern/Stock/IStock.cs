using System.Collections;
using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Stock {
	[PublicAPI]
	public interface IStock<T> : IStock {
		[PublicAPI] void Remove([NotNull] T item);
		[PublicAPI] void Add([NotNull] T item);
		[NotNull, PublicAPI]
		new IEnumerable<T> GetAll();
	}

	[PublicAPI, Stock]
	public interface IStock {
		[PublicAPI] void Remove([NotNull] object item);
		[PublicAPI] void Add([NotNull] object item);
		[NotNull, PublicAPI]
		IEnumerable GetAll();
	}
}